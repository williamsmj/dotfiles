# Tmux settings

# Set XTerm key bindings
setw -g xterm-keys on

# Set colors
set-option -g default-terminal "screen-256color"

# Set reload key to r
bind r source-file ~/.tmux.conf

# Count sessions start at 1
set -g base-index 1

# Use vim bindings
setw -g mode-keys vi

# Smart pane switching with awareness of vim splits
is_vim='echo "#{pane_current_command}" | grep -iqE "(^|\/)g?(view|n?vim?)(diff)?$"'
bind -n C-h if-shell "$is_vim" "send-keys C-h" "select-pane -L"
bind -n C-j if-shell "$is_vim" "send-keys C-j" "select-pane -D"
bind -n C-k if-shell "$is_vim" "send-keys C-k" "select-pane -U"
bind -n C-l if-shell "$is_vim" "send-keys C-l" "select-pane -R"
bind -n C-\ if-shell "$is_vim" "send-keys C-\\" "select-pane -l"
# Use prefix to re-enable Ctrl-L and Ctrl-K for redraw/kill line
bind C-l send-keys 'C-l'
bind C-k send-keys 'C-k'

# # Remap window navigation to vim
# unbind-key j
# bind-key j select-pane -D 
# unbind-key k
# bind-key k select-pane -U
# unbind-key h
# bind-key h select-pane -L
# unbind-key l
# bind-key l select-pane -R

# Set the title bar
# set -g set-titles on
# set -g set-titles-string '#(whoami) :: #h :: #(curl ipecho.net/plain;echo)'

# Set status bar
set -g status-utf8 on
set -g status-bg black
set -g status-fg white
set -g status-interval 5 
set -g status-left-length 90
set -g status-right-length 60
set -g status-left "#[fg=Green]#(whoami)#[fg=white]:#[fg=blue]#(hostname -s)#[fg=white]:#[fg=yellow]#(curl ipecho.net/plain;echo) "
set -g status-justify left
set -g status-right '#[fg=Cyan]#S #[fg=white]%a %d %b %R'

# rebind prefix (C-b) to Control-s
set -g prefix C-s
unbind-key C-b
bind-key C-s select-pane -t :.+

# bind prefix D to a standard layout
bind D source-file ~/.tmux/dev

# http://evertpot.com/osx-tmux-vim-copy-paste-clipboard/
# Copy-paste integration
set-option -g default-command "reattach-to-user-namespace -l bash"

# Use vim keybindings in copy mode
setw -g mode-keys vi

# Setup 'v' to begin selection as in Vim
bind-key -t vi-copy v begin-selection
bind-key -t vi-copy y copy-pipe "reattach-to-user-namespace pbcopy"

# Update default binding of `Enter` to also use copy-pipe
unbind -t vi-copy Enter
bind-key -t vi-copy Enter copy-pipe "reattach-to-user-namespace pbcopy"

# Bind ']' to use pbpaste
bind ] run "reattach-to-user-namespace pbpaste | tmux load-buffer - && tmux paste-buffer"

# Renumber windows on close
set-option -g renumber-windows on
